// Created by Lucas Kivi - Kivix019@umn.edu
// Last edit - 10/29/2020
// Deck class for use in PerteDeTemps game

import java.util.Random;

public class Deck
{
    private Card[] cards;
    private int count;
    private static final int defaultSize = 52; // 52 is the number of cards in a standard deck of cards
    private static final int defaultRanks = 13; // 13 is the number of ranks in a standard deck of cards
    private static final int defaultSuits = 4; // 4 is the number of suits in a standard deck of cards

    public Deck() // Constructs an instance of deck with defaultSize cards
    {
        if(defaultSize < 0)
        {
            throw new IllegalStateException("No arrays of less than 0 size");
        }

        cards = new Card[defaultSize];

        int k = 0;
        for(int i = 0; i < defaultSuits; i++)
        {
            for(int j = 0; j < defaultRanks; j++)
            {
                cards[k++] = new Card(j,i);
            }
        }
        count = defaultSize; // Starts full
    }

   public Card deal() // Deal one card from top of deck
   {
       if(count != 0)
       {
           count--;
           Card temp = cards[count];
           cards[count] = null;
           return temp;
       }
       else
       {
           throw new IllegalStateException("No cards left in deck");
       }
   }


    public void shuffle()
    {
        if(count == defaultSize)
        {
            Random r = new Random();
            int j;
            Card temp;

            for(int i = defaultSize - 1; i > 0; i--)
            {
                j = Math.abs(r.nextInt() % defaultSize);
                temp = cards[i];
                cards[i] = cards[j];
                cards[j] = temp;
            }
        }
        else
        {
            throw new IllegalStateException("Cannot a deck that isn't full");
        }
    }


}
