// Created by Lucas Kivi - Kivix019@umn.edu
// Last edit - 10/29/2020
// Pile class for use in PerteDeTemps game

public class Pile
{

    private class Layer // Nested class that contains a card and remembers which card comes next
    {
        private Card card;
        private Layer next;

        private Layer(Card card, Layer next) {
            this.card = card;
            this.next = next;
        }
    }

    private Layer top; // Represents the top/first element in the pile

    public Pile() // Pile starts as null
    {
        top = null;
    }

    public void add(Card card) // Assigns card to top of pile and links it to the previous top via next
    {
        top = new Layer(card, top);
    }

    public Card draw() // Return top Card and change top
    {
        if(isEmpty())
        {
            throw new IllegalStateException("There are no cards left in the pile to draw from.");
        }
        else
        {
            Card temp = top.card;
            top = top.next;
            return temp;
        }
    }

    public boolean isEmpty()
    {
        return top == null;
    }
}
