// Created by Lucas Kivi - Kivix019@umn.edu
// Last edit - 10/29/2020
// Tableau class for use in PerteDeTemps game

public class Tableau
{

    private Deck deck;
    private Pile[] piles;
    private Card leftCard;
    private Card rightCard;
    private static final int defaultPiles = 13; // PerteDeTemps uses 13 piles
    private static final int defaultPileSize = 4; // PerteDeTemps uses 4 count piles

    public Tableau()
    {
        if(defaultPiles < 0 || defaultPileSize < 0)
        {
            throw new IllegalStateException("Trying to play game with illegal default settings");
        }

        piles = new Pile[defaultPiles];
        deck = new Deck();
        deck.shuffle();

        leftCard = rightCard = null;

        for (int j = 0; j < defaultPiles; j++) // Deals one card per pile defaultPileSize times
        {
            piles[j] = new Pile();
            for (int i = 0; i < defaultPileSize; i++)
            {
                piles[j].add(deck.deal());
            }
        }
    }

        public void play()
        {


            int p = 0; // Track which pile we are accessing
            pickLeft(p);

            while(true)
            {
                pickRight(p);
                if(leftCard.getSuit() == rightCard.getSuit())
                {
                    p = leftCard.getRank();
                }
                else
                {
                    p = rightCard.getRank();
                }

                leftCard = rightCard;
                rightCard = null;
            }
        }

        private boolean isEmpty()
        {
            for (int i = 0; i < defaultPiles; i++)
            {
                if (!piles[i].isEmpty())
                {
                    return false;
                }
            }
            return true;
        }

        private void pickLeft(int p)
        {
            if (piles[p].isEmpty())
            {
                if (!isEmpty())
                {
                    System.out.println("Pile " + (p + 1) + " is empty. We lost!");
                    System.exit(0);
                }
                else
                    {
                    System.out.println("We won!");
                    System.exit(0);
                }
            }
            else
            {
                leftCard = piles[p].draw();
                System.out.println("Got " + leftCard + " from pile " + (p + 1));
            }
        }

    private void pickRight(int p)
    {
        if (piles[p].isEmpty())
        {
            if (!isEmpty())
            {
                System.out.println("Pile " + (p + 1) + " is empty. We lost!");
                System.exit(0);
            }
            else
            {
                System.out.println("We won!");
                System.exit(0);
            }
        }
        else
        {
            rightCard = piles[p].draw();
            System.out.println("Got " + rightCard + " from pile " + (p + 1));
        }
    }

}
