// Created by Lucas Kivi - Kivix019@umn.edu
// Last edit - 10/29/2020
// Driver class for PerteDeTemps game

public class PerteDeTemps {

    public static void main(String[] args)
    {
        Tableau t = new Tableau();
        t.play();
    }

}

// Output:
/*
        Got jack (10) of diamonds from pile 1
        Got eight (7) of spades from pile 1
        Got four (3) of diamonds from pile 8
        Got ten (9) of spades from pile 4
        Got seven (6) of diamonds from pile 10
        Got king (12) of clubs from pile 7
        Got three (2) of spades from pile 13
        Got seven (6) of hearts from pile 3
        Got six (5) of hearts from pile 7
        Got five (4) of hearts from pile 7
        Got six (5) of clubs from pile 6
        Got three (2) of clubs from pile 6
        Got four (3) of hearts from pile 6
        Got queen (11) of hearts from pile 4
        Got queen (11) of clubs from pile 4
        Got nine (8) of hearts from pile 12
        Got queen (11) of diamonds from pile 9
        Got five (4) of clubs from pile 12
        Got ten (9) of hearts from pile 5
        Got two (1) of spades from pile 10
        Got jack (10) of clubs from pile 2
        Got two (1) of clubs from pile 11
        Got four (3) of spades from pile 11
        Got four (3) of clubs from pile 4
        Pile 4 is empty. We lost!

        Process finished with exit code 0
*/